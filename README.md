# 花瓣画板图片下载器
在GUI界面输入画板ID 即可下载所有画板图片，并且记录id到目录的txt里，在画板增加图片时，能够识别新增的图片。

## 特点
- 使用tk的gui界面
- 使用py2exe即可编译，生成文件在dist

## 未实现数据库功能，因此在画板中某个图片删除时，并不能识别，只能识别新增的图片。

## 代码有点乱 请随意使用 请给5星好评哦！
